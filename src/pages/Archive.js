import { Navigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import Swal from "sweetalert2";

export default function Archive() {
  const { productId } = useParams();

  useEffect(() => {
    fetch(`http://localhost:4000/products/archive/${productId}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.archieveDone) {
          Swal.fire({
            title: "Product Archive",
            icon: "success",
            text: "You successfully archive the product.",
          });
        } else {
          Swal.fire({
            title: "Failed to archive product",
            icon: "error",
            text: "There's an error in archiving product.",
          });
        }
      });
  });
  return <Navigate to="/adminDashboard" />;
}
