import { Fragment } from "react";
import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <Fragment>
      <h1>Page Not Found!</h1>
      <span>Go back to the </span>
      <Link to="/">homepage</Link>
    </Fragment>
  );
}
