import { Button, Card, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function SpecificProduct() {
  const cartPage = useNavigate();
  const { productId } = useParams();
  const [name, setName] = useState("");
  const [brand, setBrand] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [productType, setProductType] = useState("");
  const [stock, setStock] = useState(0);

  useEffect(() => {
    fetch(`http://localhost:4000/products/${productId}`)
      .then((response) => response.json())
      .then(
        (data) => {
          setName(data.name);
          setBrand(data.brand);
          setDescription(data.description);
          setPrice(data.price);
          setProductType(data.productType);
          setStock(data.stock);
        },
        [productId]
      );
    localStorage.setItem("productId", productId);
  });

  const readyToCart = () => {
    fetch(`http://localhost:4000/orders/addToCart`, {
      method: "POST",
      headers: {
        // "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data !== null) {
          Swal.fire({
            title: "Do you want this product to add to your Cart?",
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: "Yes",
            denyButtonText: `No`,
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              Swal.fire("Please fill up the quantity", "", "success");
              cartPage(`/quantity/${data}`);
            } else if (result.isDenied) {
              Swal.fire("Okay nothing happen", "", "info");
            }
          });
        }
      });
  };

  return (
    <Col className="col-8 offset-2 mt-5">
      <Card className="text-center">
        <Card.Header>{name}</Card.Header>
        <Card.Body>
          <Card.Title>Description:</Card.Title>
          <Card.Text>{description}</Card.Text>
          <Card.Title>Brand:</Card.Title>
          <Card.Text>{brand}</Card.Text>
          <Card.Title>Price:</Card.Title>
          <Card.Text>{price}</Card.Text>
          <Button variant="primary" onClick={() => readyToCart()}>
            Add to Cart
          </Button>
        </Card.Body>
        <Card.Footer className="text-muted">Stock: {stock}</Card.Footer>
      </Card>
    </Col>
  );
}
