import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import logo from "../imgs/exocare_logo.png";
import { Container, Row, Col } from "react-bootstrap";

//we use this to get the input of the user
import { useState, useEffect, useContext } from "react";

import Swal from "sweetalert2";

import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function Register() {
  //state hooks to store the values of the input field from our user
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");

  const [isActive, setIsActive] = useState(false);

  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    if (
      fullName !== "" &&
      password !== "" &&
      password2 !== "" &&
      password === password2 &&
      email !== "" &&
      username !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, password2, fullName, username]);

  //register features
  function registerUser(event) {
    event.preventDefault();

    fetch(`http://localhost:4000/users/signup`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        fullName: fullName,
        email: email,
        username: username,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data.usernameExists) {
          Swal.fire({
            title: "Your username is already taken",
            icon: "error",
            text: "Please try another username.",
          });
        } else if (user) {
          Swal.fire({
            title: "Registration Complete",
            icon: "success",
            text: "Welcome to our website!",
          });
        }
        setUser({ id: data._id, isAdmin: data.isAdmin });
      });

    // fetch(`http://localhost:4000/users/login`, {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify({
    //     username: username,
    //     password: password,
    //   }),
    // })
    //   .then((response) => response.json())
    //   .then((data) => {
    //     console.log(data);

    //     if (data.accessToken !== "empty") {
    //       localStorage.setItem("token", data.accessToken);
    //       retrieveUserDetails(data.accessToken);

    //       Swal.fire({
    //         title: "Login Successful",
    //         icon: "success",
    //         text: "Welcome to our website!",
    //       });
    //     } else {
    //       Swal.fire({
    //         title: "Authentication failed!",
    //         icon: "error",
    //         text: "Check your login details and try again.",
    //       });
    //     }
    //   });

    // const retrieveUserDetails = (token) => {
    //   fetch(`http://localhost:4000/users/profile`, {
    //     headers: {
    //       Authorization: `Bearer ${token}`,
    //     },
    //   })
    //     .then((response) => response.json())
    //     .then((data) => {
    //       console.log(data);

    //       setUser({ id: data._id, isAdmin: data.isAdmin });
    //     });
    // };
  }

  return user.id !== null ? (
    <Navigate to="/signin" />
  ) : (
    <Container>
      <Row>
        <Col className=" mt-5" xs={12} md={6}>
          <img className="product-image" src={logo} alt="exocare_logo" />
        </Col>
        <Col className=" mt-5 border-login" xs={12} md={6}>
          <Form onSubmit={registerUser} className="bg-light p-3">
            <h3>
              <strong> Sign Up </strong>
            </h3>
            <Form.Group className="mb-3" controlId="fullName">
              <Form.Label>Full Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ex. John Doe"
                value={fullName}
                onChange={(event) => setFullName(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email:</Form.Label>
              <Form.Control
                type="email"
                placeholder="Ex. johndoe@gmail.com"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="username">
              <Form.Label>Username:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Username"
                value={username}
                onChange={(event) => setUsername(event.target.value)}
                required
              />
              <Form.Text className="text-muted">
                Your username must be unique.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
              <Form.Label>Confirm your Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password2}
                onChange={(event) => setPassword2(event.target.value)}
                required
              />
            </Form.Group>

            <Button variant="primary" type="submit" disabled={!isActive}>
              Sign Up
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
