import "./App.css";
import NavBar1 from "./components/NavBar1";
import NavBar2 from "./components/Navbar2";
//import pages
import Home from "./pages/Home";
import Signin from "./pages/Login";
import Signout from "./pages/Signout";
import SignUp from "./pages/SignUp";
import AdminDashboard from "./pages/AdminDashboard";
import PageNotFound from "./pages/PageNotFound";
import AddProduct from "./pages/AddProduct";
import UpdateProduct from "./pages/UpdateProduct";
import ProductView from "./pages/ProductView";
import Archive from "./pages/Archive";
import Unarchive from "./pages/Unarchive";
import SpecificProduct from "./pages/SpecificProduct";
import AddToCart from "./pages/AddToCart";
import Purchase from "./pages/Purchase";
//
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";
import { useState, useEffect, useContext } from "react";

function App() {
  const [user, setUser] = useState({ id: null, isAdmin: false });

  //function for clearing localstorage
  const unSetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
  }, [user]);

  useEffect(() => {
    fetch(`http://localhost:4000/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        setUser({ id: data._id, isAdmin: data.isAdmin });
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unSetUser }}>
      <Router>
        <NavBar1 />
        <NavBar2 />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/signin" element={<Signin />} />
          <Route path="/signout" element={<Signout />} />
          <Route path="/signUp" element={<SignUp />} />
          <Route path="/productview" element={<ProductView />} />
          <Route path="/adminDashboard" element={<AdminDashboard />} />
          <Route path="/addproduct" element={<AddProduct />} />
          <Route
            path="/updateProducts/:productId"
            element={<UpdateProduct />}
          />
          <Route path="/archive/:productId" element={<Archive />} />
          <Route path="/unarchive/:productId" element={<Unarchive />} />
          <Route path="/product/:productId" element={<SpecificProduct />} />
          <Route path="/quantity/:orderId" element={<AddToCart />} />
          <Route path="/checkout/:orderId1" element={<Purchase />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
