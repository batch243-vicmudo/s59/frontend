import { Button, Container, Form, Nav, Navbar } from "react-bootstrap";
import logo from "../imgs/exocare.png";
import { Fragment, useContext } from "react";
import UserContext from "../UserContext";
import { NavLink } from "react-router-dom";

export default function Navbar1() {
  const { user } = useContext(UserContext);

  return (
    <Navbar className="bg-nav-1 " expand="sm">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/">
          <img src={logo} className="logo1" alt="Logo" />
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="ms-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Form className="d-flex">
              <Form.Control
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
              />
              <Button variant="outline-success">Search</Button>
            </Form>
            <Nav.Link href="#action1">Help</Nav.Link>
            {user.id !== null ? (
              <Nav.Link as={NavLink} to="/signout">
                Sign Out
              </Nav.Link>
            ) : (
              <Fragment>
                <Nav.Link as={NavLink} to="/signin">
                  Sign In
                </Nav.Link>

                <Nav.Link as={NavLink} to="/signUp">
                  Sign Up
                </Nav.Link>
              </Fragment>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
